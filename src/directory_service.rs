use crate::Result;
use crate::{Error, HandlerError};
use std::net::{SocketAddr, ToSocketAddrs};

/// Netty-rs uses a pluggable directory service to translate from an id to an IP address and
/// port number. This is provided via this trait. A [SimpleDirectoryService](SimpleDirectoryService)
/// struct is provided which translates from any type that implements [ToSocketAddrs](ToSocketAddrs).
/// This includes strings which are in the format of for example "127.0.0.1:8080", which will allow consumers to
/// easily use strings or IP addresses as identifiers. If a more complicated lookup is required
/// then implementing this trait is always avaliable.
pub trait DirectoryService<N: Send + Sync, E: HandlerError>: Send + Sync {
    /// Translates names to Socket addresses
    fn translate(&self, name: &N) -> Result<SocketAddr, E>;
}

/// Directory service that translates Strings and socket addresses to socket addresses
/// Strings have to be in the format of "127.0.0.1:8080"
#[derive(Debug, Clone)]
pub struct SimpleDirectoryService<
    S: ToSocketAddrs<Iter = std::vec::IntoIter<SocketAddr>> + Send + Sync,
> {
    _pd: std::marker::PhantomData<S>,
}

impl<S: ToSocketAddrs<Iter = std::vec::IntoIter<SocketAddr>> + Send + Sync>
    SimpleDirectoryService<S>
{
    /// Create a new [SimpleDirectoryService](SimpleDirectoryService)
    pub fn new() -> Self {
        Self {
            _pd: std::marker::PhantomData::<S>,
        }
    }
}

impl<S: ToSocketAddrs<Iter = std::vec::IntoIter<SocketAddr>> + Send + Sync> Default
    for SimpleDirectoryService<S>
{
    fn default() -> Self {
        Self::new()
    }
}

impl<S: ToSocketAddrs<Iter = std::vec::IntoIter<SocketAddr>> + Send + Sync, E: HandlerError>
    DirectoryService<S, E> for SimpleDirectoryService<S>
{
    /// Translates a `ToSocketAddrs` to the first `SocketAddr` it yields
    fn translate(&self, name: &S) -> Result<SocketAddr, E> {
        let mut sockets = name.to_socket_addrs().map_err(|_| {
            Error::directory_service_error("Could not get socket address from directory service")
        })?;
        let socket = sockets.next().ok_or_else(|| {
            Error::directory_service_error("Could not get socket address from directory service")
        })?;
        Ok(socket)
    }
}
