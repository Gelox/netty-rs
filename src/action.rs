use crate::networker::HandlerResult;
use crate::{Connection, HandlerError, NetworkContent, NetworkMessage};
use futures::future::BoxFuture;
use serde::{de::DeserializeOwned, Serialize};
use std::fmt::Debug;

type ActionFunction<T, E> = dyn FnOnce(NetworkMessage<T>, Connection<T, E>) -> BoxFuture<'static, HandlerResult<(), E>>
    + Send
    + Sync;

/// Action contains a closure that handles the communication on a channel
pub(crate) struct Action<
    T: Send + Sync + Serialize + DeserializeOwned + Eq + PartialEq + Debug + 'static,
    E: HandlerError,
>(pub(crate) Box<ActionFunction<T, E>>);

impl<T: NetworkContent, E: HandlerError> Action<T, E> {
    /// Creates a new Action
    pub fn new<
        F: FnOnce(NetworkMessage<T>, Connection<T, E>) -> BoxFuture<'static, HandlerResult<(), E>>
            + 'static
            + Send
            + Sync,
    >(
        f: F,
    ) -> Self {
        Self(Box::new(f))
    }
}
