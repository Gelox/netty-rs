use super::*;

/// `NetworkerServer` is the main API entrypoint to netty_rs together with its brother
/// `NetworkerClient`. `NetworkerServer` is a handle to the background networking threads, the
/// handle may be cloned. A `NetworkerClient` may be upgraded to a `NetworkerServer` via the
/// `upgrade_to_server` method. If a `NetworkerServer` exists the netty will act as a server.
#[derive(Debug, Clone)]
pub struct NetworkerServer<T: NetworkContent + 'static, E: HandlerError> {
    pub(crate) tx: Sender<NetPack<T, E>>,
}

pub(crate) struct NetworkerServerInternalState<T: NetworkContent, E: HandlerError, D> {
    pub(crate) name_socket_dict: HashMap<String, SocketActor<ServerSocket, T, E>>,
    pub(crate) directory_service: D,
    pub(crate) message_rx: Receiver<NetPack<T, E>>,
}

impl<T: NetworkContent, E: HandlerError> NetworkerServer<T, E> {
    /// Sends a message and then reacts to the response with the action and then returns the
    /// last message returned
    /// Note that the first message sent to a peer will be handled by their handshake, any messages
    /// sent after that may or may not be handled by the handshake as well depending on how many
    /// RTTs is involved in their handshake. If you want to send a message after the handshake make
    /// sure you are not replying to the latest message recieved from the handshake, instead create
    /// a new `NetworkMessage` since the peer will not be waiting for responses right after the
    /// handshake is finished. This might be changed in the future by adding a dedicated
    /// client handshaking API to avoid any confusion.
    pub async fn send_message<
        H: FnOnce(NetworkMessage<T>, Connection<T, E>) -> BoxFuture<'static, HandlerResult<(), E>>
            + 'static
            + Send
            + Sync,
    >(
        &self,
        message: NetworkMessage<T>,
        timeout: Option<Duration>,
        react: Option<H>,
    ) -> Result<(), E> {
        let react = react.map(|react| Action::new(react));
        let (os_tx, os_rx) = os_channel();
        if self
            .tx
            .send((message, react, timeout, os_tx))
            .await
            .is_err()
        {
            debug!("Could not send to networker");
        }
        match os_rx.await.expect("Oneshot transmitter dropped in socket") {
            Ok(()) => Ok(()),
            Err(e) => Err(e),
        }
    }

    /// Creates a `Networker<Server>` which can send and recieve messages
    /// from peers.
    /// `address` is the network socket address that the server should listen to.
    /// `directory_service` is the directory service to use in order to translate IDs to
    /// addresses.
    /// `handle_handshakes` is a closure that asynchronously takes in a `NetworkMessage` and a `Connection` and does
    /// the necessary handshaking and authentication in order to verify that a client should be
    /// used and then returns a String which serves as the identifier for the peer.
    /// `handle_messages` is a closure that asynchronously takes in a `NetworkMessage` and a
    /// `Connection` and responds to messages from already handshaken peers.
    pub async fn new_server<H, M, FH, FM>(
        address: SocketAddr,
        directory_service: impl DirectoryService<String, E> + 'static,
        handle_handshakes: H,
        handle_messages: M,
    ) -> Result<NetworkerServer<T, E>, E>
    where
        FM: Future<Output = HandlerResult<(), E>> + Send,
        FH: Future<Output = HandlerResult<String, E>> + Send,
        M: FnMut(NetworkMessage<T>, Connection<T, E>) -> FM + Send + Sync + Clone + 'static,
        H: FnMut(NetworkMessage<T>, Connection<T, E>) -> FH + Send + Sync + Clone + 'static,
    {
        let networker = NetworkerClient::new_client(directory_service).await?;
        networker
            .upgrade_to_server(address, handle_handshakes, handle_messages)
            .await
    }

    /// Leaks the networker, ensuring that all the threads stay up for server processing but not
    /// allowing any new messages to be sent out and not requiring that you keep a handle to it.
    /// This will leak all the memory of the threads associated with the networker.
    pub fn leak(self) {
        Box::leak(Box::new(self));
    }
}

/// Perform the work of the `Networker<Server>` main work thread
pub(crate) async fn networker_server_work<T: NetworkContent, E: HandlerError, D, H, M, FM, FH>(
    internal_state: &mut NetworkerServerInternalState<T, E, D>,
    listener: &mut TcpListener,
    handle_handshakes: &H,
    handle_messages: &M,
) where
    FM: Future<Output = HandlerResult<(), E>> + Send,
    FH: Future<Output = HandlerResult<String, E>> + Send,
    M: FnMut(NetworkMessage<T>, Connection<T, E>) -> FM + Send + Sync + Clone + 'static,
    H: FnMut(NetworkMessage<T>, Connection<T, E>) -> FH + Send + Sync + Clone + 'static,
    D: DirectoryService<String, E> + 'static,
{
    // TODO: Each arm in the select should likely spawn a new task to run, otherwise it
    // ends up blocking the other arms.
    tokio::select! {
        Ok((socket, _)) = listener.accept() => {
            debug!("Received a TCP connection");
            let (name, tx) = match handshake_socket(socket, handle_handshakes.clone(), handle_messages.clone()).await {
                Ok(r) => r,
                Err(e) => {
                    debug!("Could not establish contact {:?}", e);
                    return
                },
            };
            debug!("Handshake finished peer name is: {}", name);
            internal_state.name_socket_dict.insert(name, tx);
        },
        Some(bundle) = internal_state.message_rx.recv() => {
            server_process_request(bundle, &mut internal_state.name_socket_dict, &mut internal_state.directory_service, handle_messages).await
        },
    }
}

/// Process a request to send a message inside of the main work thread in the `Network<Server>`
async fn server_process_request<T: NetworkContent, E: HandlerError, D, M, FM>(
    bundle: NetPack<T, E>,
    name_socket_dict: &mut HashMap<String, SocketActor<ServerSocket, T, E>>,
    directory_service: &mut D,
    handle_messages: &M,
) where
    FM: Future<Output = HandlerResult<(), E>> + Send,
    M: FnMut(NetworkMessage<T>, Connection<T, E>) -> FM + Send + Sync + Clone + 'static,
    D: DirectoryService<String, E> + 'static,
{
    let (message, react, timeout, os_tx) = bundle;
    // Here we need to find the correct socket to send to. And we should
    // maybe allow a "ALL" option to send towards
    match name_socket_dict.get(&*message.to) {
        // There is already a recipiant connected with that name
        Some(tx) => {
            if let Err(e) = tx.send((message, react, timeout, os_tx)).await {
                debug!("{:?}", e);
            }
        }
        // No connection to provided recipiant was found
        None => {
            // TODO: We should allow consumers to provide a do_handshake
            // function which is run automatically be ran on sending a new
            // message to an uninitiated peer. This function should return
            // a Result containing the name of the peer. If no such
            // function is provided then we skip that part
            match directory_service.translate(&message.to) {
                Ok(address) => {
                    let socket = match TcpStream::connect(address).await.map_err(|_| {
                        Error::network_error(format!("Could not connect to address {:?}", address))
                    }) {
                        Ok(s) => s,
                        Err(e) => {
                            if os_tx.send(Err(e)).is_err() {
                                debug!("Could not return error send on one-shot channel");
                            }
                            return;
                        }
                    };
                    // Spawn a new thread with a new tcp connection and
                    // send the message
                    let socket_actor =
                        match SocketActor::new_server(socket, handle_messages.clone()).await {
                            Ok(socket_actor) => socket_actor,
                            Err(e) => {
                                if os_tx.send(Err(e)).is_err() {
                                    debug!("Could not return error send on one-shot channel");
                                }
                                return;
                            }
                        };
                    let name = message.to.clone();
                    if let Err(e) = socket_actor.send((message, react, timeout, os_tx)).await {
                        debug!("{:?}", e);
                    }
                    name_socket_dict.insert((*name).clone(), socket_actor);
                }
                Err(e) => {
                    if os_tx.send(Err(e)).is_err() {
                        debug!("Could not return error send on one-shot channel");
                    }
                }
            }
        }
    }
}
