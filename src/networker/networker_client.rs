use super::networker_server::networker_server_work;
use super::*;
use std::sync::Arc;
use tokio::sync::Mutex;

/// `NetworkerClient` is the main API entrypoint to netty_rs together with its brother
/// `NetworkerServer`. `NetworkerClient` is a handle to the background networking threads, the
/// handle may be cloned. A `NetworkerClient` may be upgraded to a `NetworkerServer` via the
/// `upgrade_to_server` method. If a cloned `NetworkerClient` upgrades to a server then the
/// `is_server` method can be used to check.
#[derive(Debug, Clone)]
pub struct NetworkerClient<
    T: NetworkContent + 'static,
    E: HandlerError,
    D: DirectoryService<String, E> + 'static,
> {
    tx: Sender<NetPack<T, E>>,
    upgrade_to_server_tx: Sender<OsSender<NetworkerClientInternalState<T, E, D>>>,
    is_server: Arc<Mutex<bool>>,
}

pub(crate) struct NetworkerClientInternalState<T: NetworkContent, E: HandlerError, D> {
    name_socket_dict: HashMap<String, SocketActor<ClientSocket<T, E>, T, E>>,
    directory_service: D,
    message_rx: Receiver<NetPack<T, E>>,
}

impl<T: NetworkContent, E: HandlerError, D: DirectoryService<String, E> + 'static>
    NetworkerClient<T, E, D>
{
    /// Sends a message and then reacts to the response with the action and then returns the
    /// last message returned
    /// Note that the first message sent to a peer will be handled by their handshake, any messages
    /// sent after that may or may not be handled by the handshake as well depending on how many
    /// RTTs is involved in their handshake. If you want to send a message after the handshake make
    /// sure you are not replying to the latest message recieved from the handshake, instead create
    /// a new `NetworkMessage` since the peer will not be waiting for responses right after the
    /// handshake is finished. This might be changed in the future by adding a dedicated
    /// client handshaking API to avoid any confusion.
    pub async fn send_message<
        H: FnOnce(NetworkMessage<T>, Connection<T, E>) -> BoxFuture<'static, HandlerResult<(), E>>
            + 'static
            + Send
            + Sync,
    >(
        &self,
        message: NetworkMessage<T>,
        timeout: Option<Duration>,
        react: Option<H>,
    ) -> Result<(), E> {
        let react = react.map(|react| Action::new(react));
        let (os_tx, os_rx) = os_channel();
        if self
            .tx
            .send((message, react, timeout, os_tx))
            .await
            .is_err()
        {
            debug!("Could not send to networker");
        }
        match os_rx.await.expect("Oneshot transmitter dropped in socket") {
            Ok(()) => Ok(()),
            Err(e) => Err(e),
        }
    }

    /// Creates a new `Networker<Client>` which is used for sending messages but does not respond to
    /// non-reply messages.
    /// `directory_service` is the directory service to use in order to translate IDs to
    /// addresses.
    pub async fn new_client(directory_service: D) -> Result<Self, E> {
        // Channel for sending messages to the main thread
        let (message_tx, message_rx): Channel<NetPack<T, E>> = channel(CHANNEL_SIZE);

        // Mapping peer IDs to their respective socket threads
        let name_socket_dict: HashMap<String, SocketActor<ClientSocket<T, E>, T, E>> =
            HashMap::new();

        // Upgrade networker to server channel
        let (upgrade_to_server_tx, mut upgrade_to_server_rx): Channel<
            OsSender<NetworkerClientInternalState<T, E, D>>,
        > = channel(100);

        let mut internal_state = NetworkerClientInternalState {
            name_socket_dict,
            directory_service,
            message_rx,
        };

        tokio::spawn(async move {
            loop {
                tokio::select! {
                    _ = networker_client_work(
                        &mut internal_state
                    ) => {
                    }
                    Some(os) = upgrade_to_server_rx.recv() => {
                        if os.send(internal_state).is_err() {
                            debug!("Internal error: could not send networker state on migration");
                        }
                        break;
                    }
                }
            }
        });

        Ok(NetworkerClient {
            tx: message_tx,
            upgrade_to_server_tx,
            is_server: Arc::new(Mutex::new(false)),
        })
    }

    /// Upgrades the `Networker<Client>` to a Networker<Server> which can send and recieve messages
    /// from peers. All connections that have already been started by the client will be
    /// transferred without problem to the server.
    /// `address` is the network socket address that the server should listen to.
    /// `handle_handshakes` is a closure that asynchronously takes in a `NetworkMessage` and a `Connection` and does
    /// the necessary handshaking and authentication in order to verify that a client should be
    /// used and then returns a String which serves as the identifier for the peer.
    /// `handle_messages` is a closure that asynchronously takes in a `NetworkMessage` and a
    /// `Connection` and responds to messages from already handshaken peers.
    pub async fn upgrade_to_server<M, H, FM, FH>(
        &self,
        address: SocketAddr,
        handle_handshakes: H,
        handle_messages: M,
    ) -> Result<NetworkerServer<T, E>, E>
    where
        FM: Future<Output = HandlerResult<(), E>> + Send,
        FH: Future<Output = HandlerResult<String, E>> + Send,
        M: FnMut(NetworkMessage<T>, Connection<T, E>) -> FM + Send + Sync + Clone + 'static,
        H: FnMut(NetworkMessage<T>, Connection<T, E>) -> FH + Send + Sync + Clone + 'static,
    {
        let mut is_server = self.is_server.lock().await;
        if *is_server {
            return Err(Error::already_server(
                "Can not upgrade to server because it already is",
            ));
        }

        let (upgrade_callback_tx, upgrade_callback_rx) = os_channel();
        match self.upgrade_to_server_tx.send(upgrade_callback_tx).await {
            Ok(()) => (),
            Err(_) => {
                return Err(Error::custom(String::from("Internal error could not upgrade networker due to internal channel being dropped")));
            }
        };

        let internal_state = match upgrade_callback_rx.await {
            Ok(state) => state,
            Err(_) => {
                return Err(Error::custom(String::from("Internal error could not upgrade networker due to internal channel being dropped")));
            }
        };

        let mut upgraded_sockets = HashMap::new();
        for (key, actor) in internal_state.name_socket_dict {
            let actor = actor.upgrade_to_server(handle_messages.clone()).await?;
            upgraded_sockets.insert(key, actor);
        }

        let internal_state = NetworkerServerInternalState {
            name_socket_dict: upgraded_sockets,
            directory_service: internal_state.directory_service,
            message_rx: internal_state.message_rx,
        };

        // NOTE: We are making sure to begin the migration before we look to create the tcp
        // listener, this is so that we do not accidentally consume `self` without ending the work
        // done on the Networker<Client> thread.
        let mut listener = TcpListener::bind(address).await.map_err(|e| {
            Error::network_error(format!(
                "Could not listen to address: {} due to: {:?}",
                address, e
            ))
        })?;
        tokio::spawn(async move {
            let mut internal_state = internal_state;
            loop {
                networker_server_work(
                    &mut internal_state,
                    &mut listener,
                    &handle_handshakes,
                    &handle_messages,
                )
                .await;
            }
        });
        *is_server = true;

        Ok(NetworkerServer {
            tx: self.tx.clone(),
        })
    }

    /// Returns whether a `Cloned` `NetworkerClient` has been upgraded to a server and as such
    /// changed this `NetworkerClient` to act as a server.
    pub async fn is_server(&self) -> bool {
        *self.is_server.lock().await
    }
}

/// Perform the work of the `Networker<Client>` main work thread
async fn networker_client_work<
    T: NetworkContent,
    E: HandlerError,
    D: DirectoryService<String, E> + 'static,
>(
    internal_state: &mut NetworkerClientInternalState<T, E, D>,
) {
    if let Some(bundle) = internal_state.message_rx.recv().await {
        client_process_request(
            bundle,
            &mut internal_state.name_socket_dict,
            &mut internal_state.directory_service,
        )
        .await
    }
}

/// Process a request to send a message inside of the main work thread in the `Network<Client>`
async fn client_process_request<
    T: NetworkContent,
    E: HandlerError,
    D: DirectoryService<String, E> + 'static,
>(
    bundle: NetPack<T, E>,
    name_socket_dict: &mut HashMap<String, SocketActor<ClientSocket<T, E>, T, E>>,
    directory_service: &mut D,
) {
    let (message, react, timeout, os_tx) = bundle;
    // Here we need to find the correct socket to send to. And we should
    // maybe allow a "ALL" option to send towards
    match name_socket_dict.get(&*message.to) {
        // There is already a recipiant connected with that name
        Some(tx) => {
            if let Err(e) = tx.send((message, react, timeout, os_tx)).await {
                debug!("{:?}", e);
            }
        }
        // No connection to provided recipiant was found
        None => {
            // TODO: We should allow consumers to provide a do_handshake
            // function which is run automatically be ran on sending a new
            // message to an uninitiated peer. This function should return
            // a Result containing the name of the peer. If no such
            // function is provided then we skip that part
            match directory_service.translate(&message.to) {
                Ok(address) => {
                    let socket = match TcpStream::connect(address).await.map_err(|_| {
                        Error::network_error(format!("Could not connect to address {:?}", address))
                    }) {
                        Ok(s) => s,
                        Err(e) => {
                            if os_tx.send(Err(e)).is_err() {
                                debug!("Could not return error send on one-shot channel");
                            }
                            return;
                        }
                    };
                    // Spawn a new thread with a new tcp connection and
                    // send the message
                    let socket_actor = match SocketActor::new_client(socket).await {
                        Ok(socket_actor) => socket_actor,
                        Err(e) => {
                            if os_tx.send(Err(e)).is_err() {
                                debug!("Could not return error send on one-shot channel");
                            }
                            return;
                        }
                    };
                    let name = message.to.clone();
                    if let Err(e) = socket_actor.send((message, react, timeout, os_tx)).await {
                        debug!("{:?}", e);
                    }
                    name_socket_dict.insert((*name).clone(), socket_actor);
                }
                Err(e) => {
                    if os_tx.send(Err(e)).is_err() {
                        debug!("Could not return error send on one-shot channel");
                    }
                }
            }
        }
    }
}
