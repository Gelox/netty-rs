use crate::{
    action::Action,
    socket_processing::{
        handshake_socket, ClientSocket, NetPack, ServerSocket, SocketActor, CHANNEL_SIZE,
    },
    Connection, DirectoryService, Error, HandlerError, NetworkContent, NetworkMessage, Result,
};
use futures::future::{BoxFuture, Future};
use log::debug;
use std::{collections::HashMap, fmt::Debug, net::SocketAddr};
use tokio::{
    net::{TcpListener, TcpStream},
    sync::{
        mpsc::{channel, Receiver, Sender},
        oneshot::{channel as os_channel, Sender as OsSender},
    },
    time::Duration,
};

mod networker_client;
mod networker_server;
pub use networker_client::NetworkerClient;
pub use networker_server::NetworkerServer;
use networker_server::NetworkerServerInternalState;

pub type HandlerResult<T, E> = Result<T, E>;

pub(crate) type Channel<T> = (Sender<T>, Receiver<T>);

#[cfg(test)]
mod tester {
    use super::*;
    use crate::SimpleDirectoryService;
    use crate::*;
    use futures::FutureExt;
    use std::sync::Arc;

    #[test]
    fn networker_upgrade_to_server() {
        tokio_test::block_on(async {
            let a = Arc::new(String::from("127.0.0.1:6060"));
            let b = Arc::new(String::from("127.0.0.1:7070"));
            let ds = SimpleDirectoryService::new();
            let a_networker = NetworkerClient::<String, (), _>::new_client(ds)
                .await
                .unwrap();
            let ds = SimpleDirectoryService::new();
            let b_networker = NetworkerServer::new_server(
                "127.0.0.1:7070".parse().unwrap(),
                ds,
                |handshake: NetworkMessage<String>, mut con: Connection<String, ()>| async move {
                    con.send_message(handshake.reply(String::from("127.0.0.1:7070")))
                        .await
                        .unwrap();
                    Ok(String::from("127.0.0.1:6060"))
                },
                |msg: NetworkMessage<String>, mut con: Connection<String, ()>| async move {
                    if &msg.content != "request" {
                        panic!();
                    }
                    let ping = con
                        .send_message_await_reply(
                            msg.reply(String::from("reply")),
                            Some(Duration::from_secs(1)),
                        )
                        .await
                        .unwrap();
                    if &ping.content != "ping" {
                        panic!();
                    }
                    con.send_message(ping.reply(String::from("pong")))
                        .await
                        .unwrap();
                    Ok(())
                },
            )
            .await
            .unwrap();
            a_networker
                .send_message(
                    NetworkMessage::new(b.clone(), a.clone(), String::from("")),
                    Some(Duration::from_secs(1)),
                    Some(
                        |msg: NetworkMessage<String>, mut con: Connection<String, ()>| {
                            async move {
                                let reply = con
                                    .send_message_await_reply(
                                        NetworkMessage::new(
                                            msg.from,
                                            msg.to,
                                            String::from("request"),
                                        ),
                                        Some(Duration::from_secs(3)),
                                    )
                                    .await
                                    .unwrap();
                                if reply.content != "reply" {
                                    panic!();
                                }
                                let pong = con
                                    .send_message_await_reply(
                                        reply.reply(String::from("ping")),
                                        Some(Duration::from_secs(3)),
                                    )
                                    .await
                                    .unwrap();
                                if pong.content != "pong" {
                                    panic!();
                                }
                                Ok(())
                            }
                            .boxed()
                        },
                    ),
                )
                .await
                .unwrap();
            // Try sending a non-reply message to A
            if let Err(e) = b_networker
                .send_message(
                    NetworkMessage::new(a.clone(), b.clone(), String::from("hi")),
                    Some(Duration::from_secs(1)),
                    Some(
                        |_msg: NetworkMessage<String>, _con: Connection<String, ()>| {
                            async { Ok(()) }.boxed()
                        },
                    ),
                )
                .await
            {
                if let ErrorKind::Timeout = e.kind {
                } else {
                    panic!("Expected a timeout")
                }
            } else {
                panic!("Expected a timeout")
            }
            // Upgrade A
            let a_networker = a_networker
                .upgrade_to_server(
                    "127.0.0.1:6060".parse().unwrap(),
                    |msg: NetworkMessage<String>, _con: Connection<String, ()>| async move {
                        Ok(msg.content)
                    },
                    |msg: NetworkMessage<String>, mut con: Connection<String, ()>| async move {
                        if &msg.content != "hi" {
                            panic!();
                        }
                        con.send_message(msg.reply(String::from("ho")))
                            .await
                            .unwrap();
                        Ok(())
                    },
                )
                .await
                .unwrap();
            a_networker.leak();
            // Try sending a reply message to A
            b_networker
                .send_message(
                    NetworkMessage::new(a.clone(), b.clone(), String::from("hi")),
                    Some(Duration::from_secs(1)),
                    Some(
                        |msg: NetworkMessage<String>, _con: Connection<String, ()>| {
                            async move {
                                if &msg.content != "ho" {
                                    panic!();
                                }
                                Ok(())
                            }
                            .boxed()
                        },
                    ),
                )
                .await
                .unwrap();
        });
    }
}
