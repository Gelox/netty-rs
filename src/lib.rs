#![warn(missing_docs)]
//! Netty-rs allows exposes a simple-to-use API used to create stateful application level network
//! protocols as both a client or server.
//!
//! Netty-rs allows requires consumers specify how to handle messages in different
//! circumstances. Whenever specifying this the same API is used by using [Connection](Connection).
//! This very simple API allows consumers to specify restful protocols of varying complexity.
//! Each message-and-reply chain is in their own channel which does not and is not impacted by
//! messages sent or received in other message-and-reply chains.
//!
//! The situations where how to handle messages need to be specified are:
//! 1. When a consumer sends a message it can choose to wait for a reply and handle it in a
//! custom way.
//! 2. When acting as a server consumers need to specify how to handshake with new connections,
//! which allows custom authentication of clients among any other handshake related action.
//! 3. When acting as a server consumers need to specify how to handle non-reply messages from
//! connections that have already been authenticated.
//!
//! The main API is accessed through the [Networker](Networker) struct.
//!
//! Netty-rs uses the [DirectoryService](DirectoryService) trait in order to allow consumers to either implement
//! their own directory service for example a DNS or use the `SimpleDirectoryService` struct that
//! implements this trait.
//!
//! Example
//! ```rust
//!# use futures::FutureExt;
//!# use serde::{Serialize, Deserialize};
//!# use rand::{thread_rng, Rng};
//!# use tokio::time::Duration;
//!# use std::sync::Arc;
//!# use netty_rs::{Networker, SimpleDirectoryService, NetworkMessage, Connection};
//!# // Custom error struct
//!# #[derive(Debug, Clone)]
//!# struct MyError;
//!# fn main() -> Result<(), MyError> {
//!#   tokio_test::block_on(async {
//!fn generate_challenge() -> Vec<u8> {
//!    // Generate the challenge ...
//!#            let mut arr = [0u8; 32];
//!#            thread_rng().fill(&mut arr[..]);
//!#            arr.to_vec()
//!}
//!
//!fn verify_challenge(a: &Vec<u8>) -> bool {
//!    // Verify challenge answer ...
//!#            let mut arr = [0u8; 32];
//!#            thread_rng().fill(&mut arr[..]);
//!#            arr.to_vec();
//!#            true
//!}
//!
//!fn sign(c: &Vec<u8>) -> Vec<u8> {
//!    // Sign the challenge
//!#            let mut arr = [0u8; 32];
//!#            thread_rng().fill(&mut arr[..]);
//!#            arr.to_vec()
//!}
//!
//!// Enum for the different types of messages we want to send
//!#[derive(Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
//!enum Content {
//!    Init,
//!    Challenge(Vec<u8>),
//!    Answer(Vec<u8>),
//!    Accept,
//!    Deny,
//!    Request,
//!    Response(i32),
//!    ProtocolError,
//!}
//!
//!let ds = SimpleDirectoryService::new();
//!let networker = Networker::new_server("127.0.0.1:8080".parse().unwrap(), ds,
//!    |handshake_msg: NetworkMessage<Content>, mut con: Connection<Content, MyError>| async move {
//!       // Perhaps you authenticate by producing a challenge and then
//!       // waiting for a response
//!       let challenge = generate_challenge();
//!       let message = handshake_msg.reply(Content::Challenge(challenge));
//!       let timeout = Duration::from_secs(2);
//!       // On timeout or other errors we just abort this whole process
//!       let response = con.send_message_await_reply(message, Some(timeout)).await?;
//!       if let Content::Answer(a) = &response.content {
//!           if verify_challenge(a) {
//!               let accept_msg = response.reply(Content::Accept);
//!               con.send_message(accept_msg).await?;
//!           } else {
//!               let deny_msg = response.reply(Content::Deny);
//!               con.send_message(deny_msg).await?;
//!           }
//!       } else {
//!           let deny_msg = response.reply(Content::Deny);
//!           con.send_message(deny_msg).await?;
//!       }
//!       // Return the id of this client
//!       let inner = Arc::try_unwrap(handshake_msg.from).unwrap_or_else(|e| (*e).clone());
//!        Ok(inner)
//!    },
//!    |message: NetworkMessage<Content>, mut con: Connection<Content, MyError>| async move {
//!        if let Content::Request = message.content {
//!            // Respond with the magical number for the meaning of life
//!            let response = message.reply(Content::Response(42));
//!            con.send_message(response).await?;
//!        } else {
//!            let response = message.reply(Content::ProtocolError);
//!            con.send_message(response).await?;
//!        }
//!        Ok(())
//!    }).await.map_err(|_| MyError)?;
//!// Send a message to ourselves
//!let first_message = NetworkMessage::new(
//!    Arc::new("127.0.0.1:8080".to_string()),
//!    Arc::new("127.0.0.1:8080".to_string()),
//!    Content::Init,
//!);
//!let timeout = Duration::from_secs(2);
//!let action = |msg: NetworkMessage<Content>, mut con: Connection<Content, MyError>| {
//!        async move {
//!            if let Content::Challenge(c) = &msg.content {
//!                let answer = sign(c);
//!                let resp = msg.reply(Content::Answer(answer));
//!                let timeout = Duration::from_secs(2);
//!                let accept = con.send_message_await_reply(resp, Some(timeout)).await?;
//!                if let Content::Accept = accept.content {
//!                    let reply = con.send_message_await_reply(NetworkMessage::new(msg.to,
//!                    msg.from, Content::Request), Some(timeout)).await?;
//!                    if let Content::Response(42) = reply.content {
//!                    } else {
//!                        panic!("Incorrect response");
//!                    }
//!                    Ok(())
//!                } else {
//!                    Err(MyError.into())
//!                }
//!            } else {
//!                Err(MyError.into())
//!            }
//!        }
//!        .boxed()
//!    };
//!networker
//!    .send_message(first_message, Some(timeout), Some(action))
//!    .await.map_err(|_| MyError)?;
//!Result::<(), MyError>::Ok(())
//!#    })?;
//!#    Ok(())
//!# }
//!```

// TODO: Consider if there is ever a problem that happens because a handshake could be used for a
// new connection for a peer with an already existing connection. If this could ever happen and if
// it causes several threads for the same peer to exist.
// TODO: We should provide more information to the handshake closure such as the directory service
// as well as the IP address and similar information about the connecting client.
// TODO: We need to make the server actually multithreaded when handshaking, currently if a
// handshake is taking a long time to finish up then the server will not process message send
// requests.
// TODO: Handshaking is awkward, currently there is no way to ignore a handshake process since the
// server will always use the first message as the handshake message. Secondly the user that sends
// a message has to keep track of if the peer they are sending to has already handshook, if not
// then they will have to manually craft their message to do the handshake.
// A solution to this might be to have a handshake closure passed to the networker which is always
// run when a handshake is necessary.
// TODO: Add a retry-loop feature
// TODO: Consider making handshakes have a different content than regular messages
// TODO: If a channel dies then it will need to handshake again, this MIGHT be a problem
// TODO: Create function for handshaking on sending message
// TODO: We should verify the fields of the network message so that to and from are correct
// TODO: Make the handshake closure at least into a FnOnce, perhaps also the message closure. They
// are already cloned so they don't need to be FnMut to be called more than once
// TODO: Allow for guarantees of deliverability and interpretation by guaranteeing that errors are
// sent or acks are sent.
// TODO: Errors in the handler should sometimes or always be automatically sent on the connection
// or otherwise be collectible
mod action;
mod connection;
mod directory_service;
mod error;
mod network_message;
mod networker;
mod socket_processing;
pub use connection::Connection;
pub use directory_service::{DirectoryService, SimpleDirectoryService};
pub use error::{Error, ErrorKind, HandlerError};
pub use network_message::{NetworkContent, NetworkMessage};
pub use networker::{NetworkerClient, NetworkerServer};

/// Typedef for the result in netty-rs
pub type Result<T, E> = std::result::Result<T, Error<E>>;
