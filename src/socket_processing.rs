use crate::{
    action::Action,
    connection::Connection,
    connection::ConnectionPackage,
    error::{Error, HandlerError},
    network_message::{NetworkContent, NetworkMessage},
    networker::{Channel, HandlerResult},
    Result,
};
use futures::future::Future;
use log::debug;
use std::{collections::HashMap, sync::Arc};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::TcpStream,
    sync::{
        mpsc::{channel, Receiver, Sender},
        oneshot::{channel as os_channel, Receiver as OsReceiver, Sender as OsSender},
    },
    time::{sleep, Duration},
};

type OsChannel<T> = (OsSender<T>, OsReceiver<T>);

pub(crate) const MAX_SOCKET_BUF_SIZE: usize = 1500;
pub(crate) const DEFAULT_MESSAGE_TIMEOUT_MILLIS: u64 = 5000;
pub(crate) const CHANNEL_SIZE: usize = 100;

// TODO: Rename to NetBundle
pub(crate) type NetPack<T, E> = (
    NetworkMessage<T>,
    Option<Action<T, E>>,
    Option<Duration>,
    OsSender<Result<(), E>>,
);

type SocketStateBundle<T, E> = (
    HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
    Sender<ConnectionPackage<T, E>>,
    Receiver<ConnectionPackage<T, E>>,
    Receiver<NetPack<T, E>>,
    TcpStream,
);

pub(crate) struct ClientSocket<T: NetworkContent, E: HandlerError> {
    upgrade_to_server_channel_tx: OsSender<OsSender<SocketStateBundle<T, E>>>,
}

pub(crate) struct ServerSocket(());

pub(crate) struct SocketActor<STATE, T: NetworkContent, E: HandlerError> {
    state: STATE,
    sender: Sender<NetPack<T, E>>,
}

impl<STATE, T: NetworkContent, E: HandlerError> SocketActor<STATE, T, E> {
    pub(crate) async fn send(&self, pack: NetPack<T, E>) -> Result<(), E> {
        match self.sender.send(pack).await {
            Ok(()) => {}
            Err(_) => {
                return Err(Error::custom(String::from(
                    "Internal error could not send on channel to socket thread",
                )));
            }
        }
        Ok(())
    }
}

impl<T: NetworkContent, E: HandlerError> SocketActor<ClientSocket<T, E>, T, E> {
    /// Upgrade the Socket to act as a server as well.
    /// This means that if this TCP connection gets any messages that are not responses to an
    /// already sent message this socket will forward them. A Client socket would simply drop them.
    pub(crate) async fn upgrade_to_server<M, F>(
        self,
        mut handle_message: M,
    ) -> Result<SocketActor<ServerSocket, T, E>, E>
    where
        F: Future<Output = HandlerResult<(), E>> + Send,
        M: FnMut(NetworkMessage<T>, Connection<T, E>) -> F + Send + Sync + Clone + 'static,
    {
        let (tx, rx) = os_channel();
        match self.state.upgrade_to_server_channel_tx.send(tx) {
            Ok(()) => (),
            Err(_) => {
                return Err(Error::custom(String::from("Internal error - client socket is not upgradable due to losing upgrade channel")));
            }
        }
        let (mut awaiting_reply, mut tx, mut reaction_rx, mut rx, mut socket) = rx.await
            .map_err(|e| Error::custom(format!("Internal error - client socket is not upgradable due to losing upgrade channel: [{:?}]", e)))?;

        tokio::spawn(async move {
            debug!("Upgrading a client socket to a server socket");
            loop {
                server_socket_work(
                    &mut awaiting_reply,
                    &mut tx,
                    &mut reaction_rx,
                    &mut rx,
                    &mut socket,
                    &mut handle_message,
                )
                .await
            }
        });

        Ok(SocketActor {
            state: ServerSocket(()),
            sender: self.sender,
        })
    }

    pub(crate) async fn new_client(mut socket: TcpStream) -> Result<Self, E> {
        let (tx, mut rx): Channel<NetPack<T, E>> = channel(CHANNEL_SIZE);
        let (upgrade_to_server_channel_tx, mut upgrade_to_server_channel_rx): OsChannel<
            OsSender<SocketStateBundle<T, E>>,
        > = os_channel();
        tokio::spawn(async move {
            debug!("Starting up a new socket client");
            let (mut tx, mut reaction_rx): Channel<ConnectionPackage<T, E>> = channel(CHANNEL_SIZE);
            let mut awaiting_reply: HashMap<Arc<String>, OsSender<NetworkMessage<T>>> =
                HashMap::new();
            loop {
                tokio::select! {
                    () = client_socket_work(
                        &mut awaiting_reply,
                        &mut tx,
                        &mut reaction_rx,
                        &mut rx,
                        &mut socket,
                    ) => {
                    }
                    Ok(os) = &mut upgrade_to_server_channel_rx => {
                        os.send((awaiting_reply, tx, reaction_rx, rx, socket)).unwrap();
                        // Break out of this thread and end it
                        break;
                    }
                }
            }
        });
        Ok(Self {
            sender: tx,
            state: ClientSocket {
                upgrade_to_server_channel_tx,
            },
        })
    }
}

impl<T: NetworkContent, E: HandlerError> SocketActor<ServerSocket, T, E> {
    pub(crate) async fn new_server<M, F>(socket: TcpStream, handle_message: M) -> Result<Self, E>
    where
        F: Future<Output = HandlerResult<(), E>> + Send,
        M: FnMut(NetworkMessage<T>, Connection<T, E>) -> F + Send + Sync + Clone + 'static,
    {
        let client = SocketActor::new_client(socket).await?;
        client.upgrade_to_server(handle_message).await
    }
}

pub(crate) async fn handshake_socket<T: NetworkContent, H, M, FH, FM, E: HandlerError>(
    mut socket: TcpStream,
    mut handle_handshake: H,
    handle_message: M,
) -> Result<(String, SocketActor<ServerSocket, T, E>), E>
where
    FH: Future<Output = HandlerResult<String, E>> + Send,
    FM: Future<Output = HandlerResult<(), E>> + Send,
    H: FnMut(NetworkMessage<T>, Connection<T, E>) -> FH + Send + Sync + 'static,
    M: FnMut(NetworkMessage<T>, Connection<T, E>) -> FM + Send + Sync + Clone + 'static,
{
    let msg = read_message(&mut socket).await?;
    let (handshake_tx, mut handshake_rx) = channel(CHANNEL_SIZE);
    let con = Connection {
        sender: handshake_tx.clone(),
    };
    let listen_for_messages = async {
        //This loop never returns Ok, instead it sends successes on the channel to the
        //Connection thread in order to drive that thread forward
        loop {
            if let Some((msg, timeout, want_reply, os_tx)) = handshake_rx.recv().await {
                let msg = match serde_json::to_string(&msg) {
                    Ok(s) => s,
                    Err(_) => {
                        let e =
                            Error::serialization_error(format!("Could not serialize {:?}", msg));
                        if let Err(e) = os_tx.send(Err(e)) {
                            debug!("Oneshot return channel did not stay open: {:?}", e);
                        }
                        continue;
                    }
                };
                match socket.write_all(msg.as_bytes()).await {
                    Ok(()) => (),
                    Err(_) => {
                        let e = Error::network_error("Could not send over network socket");
                        if let Err(e) = os_tx.send(Err(e)) {
                            debug!("Oneshot return channel did not stay open: {:?}", e);
                        }
                        continue;
                    }
                };
                if !want_reply {
                    if let Err(e) = os_tx.send(Ok(None)) {
                        debug!("Oneshot return channel did not stay open: {:?}", e);
                    }
                    continue;
                } else {
                    let timeout = sleep(
                        timeout.unwrap_or(Duration::from_millis(DEFAULT_MESSAGE_TIMEOUT_MILLIS)),
                    );
                    tokio::pin!(timeout);
                    tokio::select! {
                        s = read_message(&mut socket) => {
                            match s {
                                Ok(s) => {
                                    if let Err(e) = os_tx.send(Ok(Some(s))) {
                                        debug!("Oneshot return channel did not stay open: {:?}", e);
                                    }
                                },
                                Err(e) => {
                                    if let Err(e) = os_tx.send(Err(e)) {
                                        debug!("Oneshot return channel did not stay open: {:?}", e);
                                    }
                                }
                            }
                        },
                        _ = timeout => {
                            if let Err(e) = os_tx.send(Err(Error::timeout("Did not recieve a response in time"))) {
                                debug!("Oneshot return channel did not stay open: {:?}", e);
                            }
                        }
                    }
                }
            }
        }
    };
    let r = tokio::select! {
        Result::<T, E>::Err(e) = listen_for_messages => return Err(e),
        r = handle_handshake(msg, con) => {
            match r {
                Ok(r) => r,
                Err(e) => {
                    return Err(e);
                }
            }
        }
    };
    //let tx = create_socket_task(socket, handle_message).await?;
    let tx = SocketActor::new_server(socket, handle_message).await?;
    Ok((r, tx))
}

async fn read_message<T: NetworkContent, E: HandlerError>(
    socket: &mut TcpStream,
) -> Result<NetworkMessage<T>, E> {
    let mut buf = [0; MAX_SOCKET_BUF_SIZE];
    match socket.read(&mut buf).await {
        Ok(n) => match String::from_utf8(buf[..n].to_vec()) {
            Ok(s) => match serde_json::from_str(&s) {
                Ok(s) => Ok(s),
                Err(_) => Err(Error::serialization_error(
                    "Could not deserialize recieved message",
                )),
            },
            Err(e) => Err(Error::serialization_error(format!(
                "Could not convert to utf-8 - {:?}",
                e
            ))),
        },

        Err(e) => Err(Error::network_error(format!(
            "Could not read from socket - {:?}",
            e
        ))),
    }
}

async fn socket_work_listen_main_thread<T: NetworkContent, E: HandlerError>(
    msg: NetPack<T, E>,
    tx: &mut Sender<ConnectionPackage<T, E>>,
) {
    let (msg, react, timeout, return_tx) = msg;
    debug!("On socket task - Received a message to send {:?}", msg);
    // 1. Listens to the main-thread communication channel and sends the
    //    message on the internal reaction channel to schedule it. If a
    //    reaction is passed then listens to the oneshot channel for a reply
    //    and provides this to the reaction call
    let (os_tx, os_rx) = os_channel();
    let want_reply = react.is_some();
    tx.send((msg, timeout, want_reply, os_tx))
        .await
        .expect("Networker internal error due to reaction channel being closed");
    let tx = tx.clone();
    tokio::spawn(async move {
        let r = os_rx
            .await
            .expect("Networker internal error, awaiting os_rx channel but transmitter closed");
        match r {
            Ok(r) => {
                if want_reply {
                    let react = react.expect("Networker unreachable state");
                    let msg = r.expect(
                        "Network unreachable state - received no message while expecting reply",
                    );
                    let con = Connection { sender: tx.clone() };
                    let result = react.0(msg, con).await;
                    return_tx.send(result).expect(
                        "Networker internal error - networker did not listen to return channel",
                    );
                } else {
                    return_tx.send(Ok(())).expect(
                        "Networker internal error - networker did not listen to return channel",
                    );
                }
            }
            Err(e) => {
                return_tx.send(Err(e)).expect(
                    "Networker internal error - networker did not listen to return channel",
                );
            }
        };
    });
}

async fn socket_work_client_parse_message<T: NetworkContent>(
    msg: NetworkMessage<T>,
    awaiting_reply: &mut HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
) {
    // 3) Listens to the socket and parses the data to NetworkMessages then
    //    looks in the awaiting_reply hashmap to see if there is any connection
    //    that are waiting for reply to a message that the parsed message is
    //    replying to, in that case send the message on that channel if it
    //    remains open, if the channel is closed discard the message.
    //    If there is no connection that awaits reply from this message then
    //    start a new connection for this message
    match &msg.reply {
        Some(id) => {
            match awaiting_reply.remove(id) {
                Some(tx) => {
                    debug!("Sending message to waiter {:?}", msg);
                    if let Err(e) = tx.send(msg) {
                        //Discard the channel and message
                        debug!("Discarded the message due to: {:?}", e);
                    }
                }
                None => {
                    //Discard the channel and message
                    debug!("Could not find channel to pass message on");
                }
            };
        }
        None => {
            debug!(
                "Discarded message because could not find any waiters it [{:?}]",
                msg
            );
        }
    };
}

async fn socket_work_server_parse_message<T: NetworkContent, E: HandlerError, M, F>(
    msg: NetworkMessage<T>,
    awaiting_reply: &mut HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
    tx: &mut Sender<ConnectionPackage<T, E>>,
    handle_message: &mut M,
) where
    F: Future<Output = HandlerResult<(), E>> + Send,
    M: FnMut(NetworkMessage<T>, Connection<T, E>) -> F + Send + Sync + Clone + 'static,
{
    // 3) Listens to the socket and parses the data to NetworkMessages then
    //    looks in the awaiting_reply hashmap to see if there is any connection
    //    that are waiting for reply to a message that the parsed message is
    //    replying to, in that case send the message on that channel if it
    //    remains open, if the channel is closed discard the message.
    //    If there is no connection that awaits reply from this message then
    //    start a new connection for this message
    match &msg.reply {
        Some(id) => {
            match awaiting_reply.remove(id) {
                Some(tx) => {
                    debug!("Sending message to waiter {:?}", msg);
                    if let Err(e) = tx.send(msg) {
                        //Discard the channel and message
                        debug!("Discarded the message due to: {:?}", e);
                    }
                }
                None => {
                    //Discard the channel and message
                    debug!("Could not find channel to pass message on");
                }
            };
        }
        None => {
            debug!("Did not find any waiters for {:?}", msg);
            let con = Connection { sender: tx.clone() };
            let mut handle_message = handle_message.clone();
            tokio::spawn(async move {
                if let Err(e) = handle_message(msg, con).await {
                    debug!("Handle message returned an error {:?}", e);
                }
            });
        }
    };
}

async fn socket_work_send_and_record<T: NetworkContent, E: HandlerError>(
    pack: ConnectionPackage<T, E>,
    awaiting_reply: &mut HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
    socket: &mut TcpStream,
) {
    let (msg, timeout, want_reply, os_tx) = pack;
    // 2) Send the message and then create a timeout that sends a timeout error
    //    on expiry. Also create a one-shot channel chain where we record which
    //    message we are waiting for and give it the transmission end of the
    //    chain
    debug!("Socket task - Received a request to send a message on reaction thread");
    let msg_s = match serde_json::to_string(&msg) {
        Ok(r) => r,
        Err(_) => {
            let e = Error::serialization_error(format!("Could not serialize: {:?}", msg));
            if let Err(e) = os_tx.send(Err(e)) {
                debug!("Oneshot return channel did not stay open: {:?}", e);
            }
            return;
        }
    };
    match socket.write_all(msg_s.as_bytes()).await {
        Ok(()) => (),
        Err(_) => {
            let e = Error::network_error(String::from("Could not write on socket"));
            if let Err(e) = os_tx.send(Err(e)) {
                debug!("Oneshot return channel did not stay open: {:?}", e);
            }
            return;
        }
    }
    if want_reply {
        let timeout_time = match timeout {
            Some(t) => t,
            // If no timeout is provided then we use a default
            None => Duration::from_millis(DEFAULT_MESSAGE_TIMEOUT_MILLIS),
        };
        let (hm_tx, hm_rx) = os_channel();
        awaiting_reply.insert(msg.id, hm_tx);
        tokio::spawn(async move {
            let timeout = tokio::time::sleep(timeout_time);
            tokio::pin!(timeout);
            tokio::select! {
                Ok(msg) = hm_rx => {
                    if let Err(e) = os_tx.send(Ok(Some(msg))) {
                        debug!("Oneshot return channel did not stay open: {:?}", e);
                    }
                },
                _ = timeout => {
                    if let Err(e) = os_tx.send(Err(Error::timeout("Did not recieve a response in time!"))) {
                        debug!("Oneshot return channel did not stay open: {:?}", e);
                    }
                }
            }
        });
    } else if let Err(e) = os_tx.send(Ok(None)) {
        debug!("Oneshot return channel did not stay open: {:?}", e);
    }
}

async fn client_socket_work<T: NetworkContent, E: HandlerError>(
    awaiting_reply: &mut HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
    tx: &mut Sender<ConnectionPackage<T, E>>,
    reaction_rx: &mut Receiver<ConnectionPackage<T, E>>,
    rx: &mut Receiver<NetPack<T, E>>,
    socket: &mut TcpStream,
) {
    //let mut buf = [0; MAX_SOCKET_BUF_SIZE];
    // This task is responsible for:
    // 1. Listening to the channel from the main manager thread for messages to send on
    //      this socket, sending the message out on the socket and recording which (if
    //      any) reaction was interested in responses to that message as well as
    //      recording when a timeout happens and then returning an error on the channel
    // 2. Listening to the reaction_rx channel for NetworkMessages to send and
    //    recording which message (if any) message the reaction is interested in
    //    listening to.
    // 3. Listening to the socket and parsing the data to NetworkMessages, figuring out
    //    if the parsed message is a response to any channels and in that case routing
    //    it to that channel, else calling a new handle_message instance with the
    //    message
    tokio::select! {
        Some(pack) = reaction_rx.recv() => {
            socket_work_send_and_record(pack, awaiting_reply, socket).await
        },
        Result::<NetworkMessage<T>, E>::Ok(msg) = read_message(socket) => {
            socket_work_client_parse_message(msg, awaiting_reply).await
        },
        Some(pack) = rx.recv() => {
            socket_work_listen_main_thread(pack, tx).await;
        }
    }
}

async fn server_socket_work<T: NetworkContent, E: HandlerError, M, F>(
    awaiting_reply: &mut HashMap<Arc<String>, OsSender<NetworkMessage<T>>>,
    tx: &mut Sender<ConnectionPackage<T, E>>,
    reaction_rx: &mut Receiver<ConnectionPackage<T, E>>,
    rx: &mut Receiver<NetPack<T, E>>,
    socket: &mut TcpStream,
    handle_message: &mut M,
) where
    F: Future<Output = HandlerResult<(), E>> + Send,
    M: FnMut(NetworkMessage<T>, Connection<T, E>) -> F + Send + Sync + Clone + 'static,
{
    //let mut buf = [0; MAX_SOCKET_BUF_SIZE];
    // This task is responsible for:
    // 1. Listening to the channel from the main manager thread for messages to send on
    //      this socket, sending the message out on the socket and recording which (if
    //      any) reaction was interested in responses to that message as well as
    //      recording when a timeout happens and then returning an error on the channel
    // 2. Listening to the reaction_rx channel for NetworkMessages to send and
    //    recording which message (if any) message the reaction is interested in
    //    listening to.
    // 3. Listening to the socket and parsing the data to NetworkMessages, figuring out
    //    if the parsed message is a response to any channels and in that case routing
    //    it to that channel, else calling a new handle_message instance with the
    //    message
    tokio::select! {
        Some(pack) = reaction_rx.recv() => {
            socket_work_send_and_record(pack, awaiting_reply, socket).await
        },
        Result::<NetworkMessage<T>, E>::Ok(msg) = read_message(socket) => {
            socket_work_server_parse_message(msg, awaiting_reply, tx, handle_message).await
        },
        Some(pack) = rx.recv() => {
            socket_work_listen_main_thread(pack, tx).await;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::error::*;
    use futures::FutureExt;
    use tokio::net::{TcpStream, *};
    #[test]
    fn socket_actors_upgrade_to_server() {
        tokio_test::block_on(async {
            let tcp = TcpListener::bind("127.0.0.1:8080").await.unwrap();
            let b_stream = TcpStream::connect("127.0.0.1:8080").await.unwrap();
            let (a_stream, _) = tcp.accept().await.unwrap();
            let a = Arc::new(String::from("A"));
            let b = Arc::new(String::from("B"));
            let a_client: SocketActor<ClientSocket<String, ()>, _, ()> =
                SocketActor::new_client(a_stream).await.unwrap();
            let b_server: SocketActor<ServerSocket, _, ()> =
                SocketActor::new_server(b_stream, |msg_from_a, mut con| async move {
                    if &msg_from_a.content == "request" {
                        let msg = msg_from_a.reply(String::from("response from B"));
                        con.send_message(msg).await.unwrap()
                    }
                    Ok(())
                })
                .await
                .unwrap();

            // Send message from A to B, await a response
            let msg = NetworkMessage::new(b.clone(), a.clone(), String::from("request"));
            let (tx, rx) = os_channel();
            let (msg_tx, msg_rx) = os_channel();
            a_client
                .send((
                    msg,
                    Some(Action::new(|msg, _con| {
                        async {
                            msg_tx.send(msg).unwrap();
                            Ok(())
                        }
                        .boxed()
                    })),
                    Some(Duration::from_secs(1)),
                    tx,
                ))
                .await
                .unwrap();
            rx.await.unwrap().unwrap();
            let response = msg_rx.await.unwrap();
            assert_eq!(&response.content, "response from B");

            // Send message from B to A, await non-response
            let msg = NetworkMessage::new(a.clone(), b.clone(), String::from("request"));
            let (tx, rx) = os_channel();
            let (msg_tx, msg_rx) = os_channel();
            b_server
                .send((
                    msg,
                    Some(Action::new(|msg, _con| {
                        async {
                            msg_tx.send(msg).unwrap();
                            Ok(())
                        }
                        .boxed()
                    })),
                    Some(Duration::from_secs(1)),
                    tx,
                ))
                .await
                .unwrap();
            if let Ok(Err(resp)) = rx.await {
                match &resp.kind {
                    ErrorKind::Timeout => {}
                    _ => {
                        panic!("Got wrong error {:?}", resp);
                    }
                }
            } else {
                panic!("Got response when not supposed to");
            }
            if let Ok(_) = msg_rx.await {
                panic!("Got response when not supposed to");
            }

            // Upgrade A to server
            let a_server = a_client
                .upgrade_to_server(|msg_from_b, mut con| async move {
                    if &msg_from_b.content == "request" {
                        let msg = msg_from_b.reply(String::from("response from A"));
                        con.send_message(msg).await.unwrap()
                    }
                    Ok(())
                })
                .await
                .unwrap();

            // Send message from B to A, await response
            let msg = NetworkMessage::new(a.clone(), b.clone(), String::from("request"));
            let (tx, rx) = os_channel();
            let (msg_tx, msg_rx) = os_channel();
            b_server
                .send((
                    msg,
                    Some(Action::new(|msg, _con| {
                        async {
                            msg_tx.send(msg).unwrap();
                            Ok(())
                        }
                        .boxed()
                    })),
                    Some(Duration::from_secs(1)),
                    tx,
                ))
                .await
                .unwrap();
            rx.await.unwrap().unwrap();
            let response = msg_rx.await.unwrap();
            assert_eq!(&response.content, "response from A");

            // Send message from A to B, await response, make sure A can still send messages
            let msg = NetworkMessage::new(b.clone(), a.clone(), String::from("request"));
            let (tx, rx) = os_channel();
            let (msg_tx, msg_rx) = os_channel();
            a_server
                .send((
                    msg,
                    Some(Action::new(|msg, _con| {
                        async {
                            msg_tx.send(msg).unwrap();
                            Ok(())
                        }
                        .boxed()
                    })),
                    Some(Duration::from_secs(1)),
                    tx,
                ))
                .await
                .unwrap();
            rx.await.unwrap().unwrap();
            let response = msg_rx.await.unwrap();
            assert_eq!(&response.content, "response from B");
        });
    }
}
