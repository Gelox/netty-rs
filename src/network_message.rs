use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::{fmt::Debug, sync::Arc};

/// Marker trait for the content contained in the [NetworkMessage](NetworkMessage) struct
pub trait NetworkContent:
    Serialize + DeserializeOwned + Send + Sync + Eq + PartialEq + 'static + Debug
{
}

impl<T> NetworkContent for T where
    T: Serialize + DeserializeOwned + Send + Sync + Eq + PartialEq + 'static + Debug
{
}

/// This struct represents the network messages sent and received, it can be created either from
/// the new `new` constructor if a fresh message is desired.
/// If a reply is desired then the `reply` method should be used.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub struct NetworkMessage<T: NetworkContent> {
    /// The receiver id, this is currently a string but this may change to a generic representing
    /// an id in the future
    pub to: Arc<String>,
    /// The sender id, this is currently a string but this may change to a generic representing
    /// an id in the future
    pub from: Arc<String>,
    /// The message id, used to reply to this message
    pub id: Arc<String>,
    /// If this is set to `Some` then this message is a reply to the contained ID, else it is a
    /// fresh message
    pub reply: Option<Arc<String>>,
    /// The content of the message
    #[serde(bound(deserialize = "T: DeserializeOwned"))]
    pub content: T,
}

fn new_id() -> String {
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect();
    rand_string
}

impl<T: NetworkContent> NetworkMessage<T> {
    /// Create a new fresh message
    pub fn new(to: Arc<String>, from: Arc<String>, content: T) -> Self {
        Self {
            to,
            from,
            id: Arc::new(new_id()),
            reply: None,
            content,
        }
    }

    /// Used to construct replies to the provided message.
    pub fn reply(&self, content: T) -> Self {
        Self {
            to: self.from.clone(),
            from: self.to.clone(),
            id: Arc::new(new_id()),
            reply: Some(self.id.clone()),
            content,
        }
    }
}
