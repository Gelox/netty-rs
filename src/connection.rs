use crate::{HandlerError, NetworkContent, NetworkMessage, Result};
use tokio::{
    sync::{
        mpsc::Sender,
        oneshot::{channel as os_channel, Sender as OsSender},
    },
    time::Duration,
};

pub type ConnectionPackage<T, E> = (
    NetworkMessage<T>,
    Option<Duration>,
    bool,
    OsSender<Result<Option<NetworkMessage<T>>, E>>,
);

/// A connection serves as the main API to specify how a network conversation should look. It
/// allows sending messages to the recipiant and awaiting their response using the two methods
/// `send_message` and `send_message_await_reply`.
pub struct Connection<T: NetworkContent, E: HandlerError> {
    pub(crate) sender: Sender<ConnectionPackage<T, E>>,
}

impl<T: NetworkContent, E: HandlerError> Connection<T, E> {
    /// Send a message over the connection without waiting for a reply, returning upon successfully
    /// sending the message out.
    pub async fn send_message(&mut self, msg: NetworkMessage<T>) -> Result<(), E> {
        let (tx, rx) = os_channel();
        match self.sender.send((msg, None, false, tx)).await {
            Ok(()) => (),
            Err(_) => {
                panic!("Internal error - could not send on internal channel",);
            }
        };
        let r = match rx.await {
            Ok(r) => r,
            Err(_) => {
                panic!("Internal error - internal return channel was closed before receiving a message");
            }
        };
        r.map(|r| {
            if r.is_some() {
                panic!("Unreachable state - expected None but was provided a network message")
            }
        })
    }

    /// Send a message over the connection while waiting for a reply, a `Result` is returned with
    /// the replying `NetworkMessage`.
    pub async fn send_message_await_reply(
        &mut self,
        msg: NetworkMessage<T>,
        timeout: Option<Duration>,
    ) -> Result<NetworkMessage<T>, E> {
        let (tx, rx) = os_channel();
        match self.sender.send((msg, timeout, true, tx)).await {
            Ok(()) => (),
            Err(_) => {
                //return Err(Error::custom(
                //    "Internal error - could not send on internal channel",
                //));
                panic!("Internal error - could not send on internal channel",);
            }
        };
        let r = match rx.await {
            Ok(r) => r,
            Err(_) => {
                //return Err(Error::custom("Internal error - internal return channel was closed before receiving a message"));
                panic!("Internal error - internal return channel was closed before receiving a message");
            }
        };
        r.map(|r| r.expect("Expecting a network message as response but None was provided"))
    }
}
