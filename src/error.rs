use std::fmt::Debug;

/// Marker trait for errors that are returned by the handlers
pub trait HandlerError: Send + Sync + Debug + 'static + Clone {}

impl<T> HandlerError for T where T: Send + Sync + Debug + 'static + Clone {}

/// Errorkind in the error of netty-rs
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum ErrorKind<E: HandlerError> {
    /// Error when an operation timed out
    Timeout,
    /// Error returned when the protocol has not been followed correctly
    ProtocolBreak,
    /// Generic error
    Unspecified,
    /// Serialization did not succeed
    SerializationError,
    /// Something was not found
    NotFound,
    /// Error with the directory service
    DirectoryService,
    /// Could not upgrade to server due to already being a server
    AlreadyServer,
    /// Error returned by a network handler closure
    HandlerError(E),
}

/// Error returned by netty-rs
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Error<E: HandlerError> {
    /// The kind of error
    pub kind: ErrorKind<E>,
    /// The message provided with the error
    pub msg: String,
}

impl<T: HandlerError> From<T> for Error<T> {
    fn from(e: T) -> Self {
        Self::handler_error(e)
    }
}

impl<E: HandlerError> Error<E> {
    pub(crate) fn handler_error(e: E) -> Self {
        Self {
            kind: ErrorKind::HandlerError(e),
            msg: "Handler returned an error".to_string(),
        }
    }

    pub(crate) fn timeout<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::Timeout,
            msg: msg.to_string(),
        }
    }

    pub(crate) fn custom<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::Unspecified,
            msg: msg.to_string(),
        }
    }

    pub(crate) fn serialization_error<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::SerializationError,
            msg: msg.to_string(),
        }
    }

    pub(crate) fn directory_service_error<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::DirectoryService,
            msg: msg.to_string(),
        }
    }

    pub(crate) fn network_error<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::DirectoryService,
            msg: msg.to_string(),
        }
    }

    pub(crate) fn already_server<S: ToString>(msg: S) -> Self {
        Self {
            kind: ErrorKind::AlreadyServer,
            msg: msg.to_string(),
        }
    }
}
