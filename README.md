# Netty-rs
Netty-rs exposes a simple-to-use API used to create stateful application level network
protocols as both a client or server.

# Documentation
[Documentation](https://docs.rs/netty-rs/0.1.0/netty_rs/) can be found on docs.rs together with a working example.
There is also a [crates.io](https://crates.io/crates/netty-rs) link

# Motivation
When implementing p2p applications I found myself implementing the same TCP constructs over and over again in order to be able to implement a custom message passing protocol that allowed for multiplexing different peers and replying to messages.
This crate is an attempt to generalize the solution to this problem.

# API
Netty-rs requires consumers specify how to handle messages in different
circumstances. Whenever specifying this the same API is used by using `Connection`.
This very simple API allows consumers to specify restful protocols of varying complexity.
Each message-and-reply chain is in their own channel which does not and is not impacted by
messages sent or received in other message-and-reply chains.

The situations where how to handle messages need to be specified are:
1. When a consumer sends a message it can choose to wait for a reply and handle it in a
custom way.
2. When acting as a server consumers need to specify how to handshake with new connections,
which allows custom authentication of clients among any other handshake related action.
3. When acting as a server consumers need to specify how to handle non-reply messages from
connections that have already been authenticated.

The main API is accessed through the Networker struct.

Netty-rs uses the `DirectoryService` trait in order to allow consumers to either implement
their own directory service for example a DNS or use the `SimpleDirectoryService` struct that
implements this trait.

See the docs for further documentation and an example.
